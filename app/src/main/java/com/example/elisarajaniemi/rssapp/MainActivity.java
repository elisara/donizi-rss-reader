package com.example.elisarajaniemi.rssapp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;


public class MainActivity extends AppCompatActivity { //

    private HandleXML obj;
    ListView listView;
    Urls url = new Urls();
    User user = User.getInstance();
    RSSItem bookmark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.feed);

        int i = 0;
        while (i < url.all.size()) {
            obj = new HandleXML(url.all.get(i));
            obj.fetchXML();
            i++;

            while (!obj.parsingComplete) ;
            ArrayAdapter<RSSItem> adapter = new ArrayAdapter<>(this, R.layout.list_item, R.id.textView1, user.getFeeds());
            listView.setAdapter(adapter);
            sortByDate();
        }
        goToFeed();
        bookMark();

    }

    public void sortByDate() {
        Collections.sort(user.feeds, new Comparator<RSSItem>() {
            @Override
            public int compare(RSSItem rss1, RSSItem rss2) {
                if (rss1.getDate() < rss2.getDate())
                    return 1;
                if (rss1.getDate() > rss2.getDate())
                    return -1;
                return 0;
            }
        });
    }

    public void goToFeed() {
        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                String linkPosition = user.getLink(position);
                user.feeds.get(position).changeTitle();
                System.out.println(user.feeds.get(position).toString());
                v.setSelected(true);

                Bundle bundle = new Bundle();
                bundle.putString("content", linkPosition);
                Intent intent = new Intent(MainActivity.this, Second.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }

    //PÄIVITYS
    public void all(View view) {
        recreate();
    }

    //UUTISET
    public void news(View view) {
        user.feeds.clear();
        listView = (ListView) findViewById(R.id.feed);

        int i = 0;
        while (i < url.news.size()) {
            obj = new HandleXML(url.all.get(i));
            obj.fetchXML();
            i++;

            while (!obj.parsingComplete) ;
            ArrayAdapter<RSSItem> adapter = new ArrayAdapter<>(this, R.layout.list_itemnews, R.id.textView1, user.getFeeds());
            listView.setAdapter(adapter);
            sortByDate();
        }
        goToFeed();
        bookMark();
    }

    //URHEILU
    public void sports(View view) {
        user.feeds.clear();
        listView = (ListView) findViewById(R.id.feed);

        int i = 0;
        while (i < url.sports.size()) {
            obj = new HandleXML(url.sports.get(i));
            obj.fetchXML();
            i++;

            while (!obj.parsingComplete) ;
            ArrayAdapter<RSSItem> adapter = new ArrayAdapter<>(this, R.layout.list_itemsport, R.id.textView1, user.getFeeds());
            listView.setAdapter(adapter);
            sortByDate();
        }
        goToFeed();
        bookMark();
    }

    //VIIHDE
    public void viihde(View view) {
        user.feeds.clear();
        listView = (ListView) findViewById(R.id.feed);

        int i = 0;
        while (i < url.viihde.size()) {
            obj = new HandleXML(url.viihde.get(i));
            obj.fetchXML();
            i++;

            while (!obj.parsingComplete) ;
            ArrayAdapter<RSSItem> adapter = new ArrayAdapter<>(this, R.layout.list_itemviihde, R.id.textView1, user.getFeeds());
            listView.setAdapter(adapter);
            sortByDate();
        }
        goToFeed();
        bookMark();
    }

    //POLITIIKKA JA TALOUS
    public void politic(View view) {
        user.feeds.clear();
        listView = (ListView) findViewById(R.id.feed);
        int i = 0;
        while (i < url.politic.size()) {
            obj = new HandleXML(url.politic.get(i));
            obj.fetchXML();
            i++;

            while (!obj.parsingComplete) ;
            ArrayAdapter<RSSItem> adapter = new ArrayAdapter<>(this, R.layout.list_itempolitic, R.id.textView1, user.getFeeds());
            listView.setAdapter(adapter);
            sortByDate();
        }
        goToFeed();
        bookMark();
    }

    //TEKNIIKKA
    public void tech(View view) {
        user.feeds.clear();
        listView = (ListView) findViewById(R.id.feed);
        int i = 0;
        while (i < url.tech.size()) {
            obj = new HandleXML(url.tech.get(i));
            obj.fetchXML();
            i++;

            while (!obj.parsingComplete) ;
            ArrayAdapter<RSSItem> adapter = new ArrayAdapter<>(this, R.layout.list_itemtech, R.id.textView1, user.getFeeds());
            listView.setAdapter(adapter);
            sortByDate();
        }
        goToFeed();
        bookMark();
    }

    //HEALTH
    public void health(View view) {
        user.feeds.clear();
        listView = (ListView) findViewById(R.id.feed);
        int i = 0;
        while (i < url.health.size()) {
            obj = new HandleXML(url.health.get(i));
            obj.fetchXML();
            i++;

            while (!obj.parsingComplete) ;
            ArrayAdapter<RSSItem> adapter = new ArrayAdapter<>(this, R.layout.list_itemhealth, R.id.textView1, user.getFeeds());
            listView.setAdapter(adapter);
            sortByDate();
        }
        goToFeed();
        bookMark();
    }

    public void bookMark() {

        listView.setOnItemLongClickListener(new android.widget.AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapter, View v, int position, long id) {
                bookmark = user.feeds.get(position);

                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle(R.string.bookmarkDialog);
                alert.setMessage(R.string.bookmarkMessage);

                alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        user.bookmark.add(bookmark);
                    }
                });
                alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                alert.show();

                return true;
            }

        });

    }

    //MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int option = item.getItemId();
        switch (option) {
            case R.id.like:


                listView = (ListView) findViewById(R.id.feed);
                ArrayAdapter<RSSItem> adapter = new ArrayAdapter<>(this, R.layout.list_itembookmark, R.id.textView1, user.getBookmark());
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                        String linkPosition = user.getBookmarkLink(position);
                        v.setSelected(true);
                        Bundle bundle = new Bundle();
                        bundle.putString("content", linkPosition);
                        Intent intent = new Intent(MainActivity.this, Second.class);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });

                //POISTA BOOKMARKEISTA
                listView.setOnItemLongClickListener(new android.widget.AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapter, View v, int position, long id) {
                        bookmark = user.bookmark.get(position);
                        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                        alert.setTitle(R.string.deleteDialog);
                        alert.setMessage(R.string.deleteMessage);
                        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                user.bookmark.remove(bookmark);
                                recreate();
                            }
                        });
                        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });
                        alert.show();
                        return true;
                    }
                });
                break;

            case R.id.donut:
                Toast.makeText(this, R.string.donut, Toast.LENGTH_LONG).show();
                break;
        }
        return false;
    }

    //QUIT & DIALOG
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
            alert.setTitle(R.string.quitDialog);
            alert.setMessage(R.string.quitMessage);

            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    MainActivity.this.finish();
                }
            });
            alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });
            alert.show();
        }
        return true;
    }
}





