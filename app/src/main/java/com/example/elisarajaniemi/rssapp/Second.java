package com.example.elisarajaniemi.rssapp;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;


//WEBVIEW

public class Second extends AppCompatActivity {

    private String postContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_act);

        Bundle bundle = this.getIntent().getExtras();
        postContent = bundle.getString("content");
        System.out.println(postContent);


        WebView w1 = (WebView) findViewById(R.id.rss_feed);
        w1.setWebViewClient(new WebViewClient());

        w1.getSettings().setJavaScriptEnabled(true);
        w1.getSettings().setLoadsImagesAutomatically(true);
        w1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        w1.loadData(postContent, "text/html; charset=utf-8", "utf-8");
        w1.loadUrl(postContent);
    }

        private class MyCustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}

