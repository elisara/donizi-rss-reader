package com.example.elisarajaniemi.rssapp;


import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;


public class HandleXML {
    
    private String title, link, pubDate, image;
    private long timeInMinutesSinceEpoch;
    private String url1 = null;
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingComplete = false;
    User user = User.getInstance();

    public HandleXML(String url1) {

        this.url1 = url1;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        boolean insideItem = false;

        try {
            event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myParser.getName();

                if (event == XmlPullParser.START_TAG) {
                    if (name.equalsIgnoreCase("item")) {
                        insideItem = true;
                    }
                    else if(name.equalsIgnoreCase("url")){
                        insideItem = true;
                    }
                    else if (name.equalsIgnoreCase("title")) {
                        if (insideItem)
                            this.title = myParser.nextText();
                    }
                    else if (name.equalsIgnoreCase("link")) {
                        if (insideItem)
                            this.link = myParser.nextText();
                    }
                    else if (name.equalsIgnoreCase("url")) {
                        if (insideItem)
                            this.image = myParser.nextText();
                    }
                    else if (name.equalsIgnoreCase("pubDate")) {
                        if (insideItem)
                            this.pubDate = myParser.nextText();
                        try{
                            SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss z");
                            Date date = sdf.parse(this.pubDate);
                            long timeInMillisSinceEpoch = date.getTime();
                            this.timeInMinutesSinceEpoch = timeInMillisSinceEpoch / (60*1000);
                        }catch(Exception e){

                        }
                    }

                } else if (event == XmlPullParser.END_TAG && name.equalsIgnoreCase("item")) {
                        RSSItem rss = new RSSItem(title, link, timeInMinutesSinceEpoch);
                        if(user.feeds.size()<=30){
                            user.addFeeds(rss);
                        }
                    insideItem = false;
                }
                event = myParser.next();
            }
            parsingComplete = true;

        } catch (Exception e) { }
    }

    public void fetchXML() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(url1);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setConnectTimeout(1500);
                    con.setRequestMethod("GET");
                    con.setDoInput(true);
                    con.connect();
                    InputStream stream = con.getInputStream();

                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    XmlPullParser myParser = xmlFactoryObject.newPullParser();
                    myParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myParser.setInput(stream, null);
                    parseXMLAndStoreIt(myParser);
                    stream.close();

                } catch (Exception e) {
                }
            }
        });
        thread.start();
    }
}