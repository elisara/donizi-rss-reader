package com.example.elisarajaniemi.rssapp;

import java.util.ArrayList;

/**
 * Created by Elisa Rajaniemi on 2.10.2015.
 */
public class User {

    private static User instance = new User();

    private User(){}

    public static User getInstance(){

        return instance;
    }
    public ArrayList<RSSItem> feeds = new ArrayList<>(30);
    public ArrayList<RSSItem> bookmark = new ArrayList<>();

    public void addFeeds(RSSItem rssItem){

        feeds.add(rssItem);
    }

    public ArrayList<RSSItem> getFeeds(){

        return feeds;
    }

    public String getLink(int position){

        return feeds.get(position).getLink();
    }

    public ArrayList<RSSItem> getBookmark(){

        return bookmark;
    }
    public String getBookmarkLink(int position){

        return bookmark.get(position).getLink();
    }


}
