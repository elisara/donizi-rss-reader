package com.example.elisarajaniemi.rssapp;

/**
 * Created by Elisa Rajaniemi on 24.9.2015.
 */
public class RSSItem {

    private String title, link;
    private long pubDate;


    public RSSItem(String title, String link, long pubDate){
        this.title = title;
        this.link = link;
        this.pubDate = pubDate;

    }
    @Override
    public String toString() {

        return this.title;
    }
    public String getLink(){

        return this.link;
    }
    public Long getDate(){

        return this.pubDate;
    }
    public String changeTitle(){
        this.title = "LUETTU!\n " + this.title;
        return this.title;
    }
}
