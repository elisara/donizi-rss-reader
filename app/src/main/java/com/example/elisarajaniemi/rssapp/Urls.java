package com.example.elisarajaniemi.rssapp;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Elisa Rajaniemi on 30.9.2015.
 */
public class Urls {

    public ArrayList<String> all = new ArrayList<>();
    public ArrayList<String> news = new ArrayList<>();
    public ArrayList<String> sports = new ArrayList<>();
    public ArrayList<String> viihde = new ArrayList<>();
    public ArrayList<String> tech = new ArrayList<>();
    public ArrayList<String> politic = new ArrayList<>();
    public ArrayList<String> health = new ArrayList<>();

    public Iterator iterator = all.iterator();

    public Urls() {
        all.add("http://yle.fi/uutiset/rss/uutiset.rss");
        all.add("http://www.iltasanomat.fi/rss/tuoreimmat.xml");
        all.add("http://www.mtv.fi/api/feed/rss/uutiset_uusimmat");
        all.add("http://yle.fi/uutiset/rss/uutiset.rss?osasto=terveys");
        all.add("http://www.iltasanomat.fi/rss/hyvaolo.xml");
        all.add("http://yle.fi/uutiset/rss/uutiset.rss?osasto=politiikka");
        all.add("http://www.mtv.fi/api/feed/rss/uutiset_talous");
        all.add("http://yle.fi/uutiset/rss/uutiset.rss?osasto=pelit");
        all.add("http://www.mtv.fi/api/feed/rss/uutiset_it");
        all.add("http://yle.fi/uutiset/rss/uutiset.rss?osasto=viihde");
        all.add("http://www.iltasanomat.fi/rss/viihde.xml");
        all.add("http://www.mtv.fi/api/feed/rss/viihde_uusimmat_100");
        all.add("http://www.iltasanomat.fi/rss/urheilu.xml");
        all.add("http://www.mtv.fi/api/feed/rss/urheilu");

        news.add("http://yle.fi/uutiset/rss/uutiset.rss");
        news.add("http://www.iltasanomat.fi/rss/tuoreimmat.xml");
        news.add("http://www.mtv.fi/api/feed/rss/uutiset_uusimmat");

        health.add("http://yle.fi/uutiset/rss/uutiset.rss?osasto=terveys");
        health.add("http://www.iltasanomat.fi/rss/hyvaolo.xml");

        politic.add("http://yle.fi/uutiset/rss/uutiset.rss?osasto=politiikka");
        politic.add("http://www.mtv.fi/api/feed/rss/uutiset_talous");

        tech.add("http://yle.fi/uutiset/rss/uutiset.rss?osasto=pelit");
        tech.add("http://www.mtv.fi/api/feed/rss/uutiset_it");

        viihde.add("http://yle.fi/uutiset/rss/uutiset.rss?osasto=viihde");
        viihde.add("http://www.iltasanomat.fi/rss/viihde.xml");

        sports.add("http://www.iltasanomat.fi/rss/urheilu.xml");
        sports.add("http://www.mtv.fi/api/feed/rss/urheilu");





    }

    public boolean getNext(){

        return iterator.hasNext();
    }
}
